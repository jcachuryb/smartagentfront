import React from "react";
import { connect } from "react-redux";

const LoggedMessage = ({ isLoggedIn }) => (

    <h2 className="bold"> El usuario está conectado? {isLoggedIn ? ' Sísas' : 'Nope'} </h2>
)

const mapStateToProps = state => {
    const { auth } = state;
    return { isLoggedIn: auth ? auth.loggedIn : false };
}



export default connect(mapStateToProps)(LoggedMessage);