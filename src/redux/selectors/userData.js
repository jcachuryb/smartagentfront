export const getAuthState = store => store.auth;


export const getAccessToken = store => {
    const authState = getAuthState(store);
    if (!authState) {
        return null;
    }
    const { accessToken } = authState;
    return accessToken;
}

export const getRefreshToken = store => {
    const authState = getAuthState(store);
    if (!authState) {
        return null;
    }
    const { accessToken } = authState;
    return accessToken;
}

export const isLoggedIn = store => {
    const authState = getAuthState(store);
    return authState && authState.isLoggedIn;
}

// export const getTodoList = store =>
//     getTodosState(store) ? getTodosState(store).allIds : [];

// export const getTodoById = (store, id) =>
//     getTodosState(store) ? { ...getTodosState(store).byIds[id], id } : {};

// /**
//  * example of a slightly more complex selector
//  * select from store combining information from multiple reducers
//  */
// export const getTodos = store =>
//     getTodoList(store).map(id => getTodoById(store, id));

// export const getTodosByVisibilityFilter = (store, visibilityFilter) => {
//     const allTodos = getTodos(store);
//     switch (visibilityFilter) {
//         case VISIBILITY_FILTERS.COMPLETED:
//             return allTodos.filter(todo => todo.completed);
//         case VISIBILITY_FILTERS.INCOMPLETE:
//             return allTodos.filter(todo => !todo.completed);
//         case VISIBILITY_FILTERS.ALL:
//         default:
//             return allTodos;
//     }
// };
