import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import clientAPI from "../../connect/myAPI";


const initialState = {
    username: "Disconnected",
    loggedIn: false,
    accessToken: null,
    error: null
}

export const doLogin = createAsyncThunk(
    '/api/auth/login',
    async (authData, { rejectWithValue }) => {
        try {
            const { username, password } = authData;
            const response = await clientAPI.doLogin(username, password);
            return await response;

        } catch (err) {
            let error = err;
            if (!error.response) {
                throw err;
            }
            return rejectWithValue(error.response.data);
        }
    }
)



const authSlice = createSlice({
    name: 'login',
    initialState,
    reducers: {

    },

    extraReducers: {
        [doLogin.fulfilled.type] : (state, action) => {

            const {success, message, data} = action.payload;
            if (success && data) {
                state.loggedIn = true;
                state.accessToken = data.accessToken;
                
            } else {
                state.loggedIn = false;
                state.error =  {
                    message: "Username or Password don't exist."
                }
            }
            // Store somewhere AccessToken
        },
        [doLogin.rejected.type] : (state, action) => {

            console.log("rejected")
        }
    }
})


export default authSlice.reducer;