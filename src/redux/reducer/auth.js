import { ADD_TODO, TOGGLE_TODO, LOGIN, LOGOUT, LOAD_USER } from "../actionTypes";

const initialState = {
    allIds: [],
    byIds: {},

    isLoggedIn: false,
    loaded: false,
    accessToken: null,
    refreshToken: null
};

export default function (state = initialState, action) {
    switch (action.type) {
        case LOGIN:
            const { accessToken, refreshToken } = action.payload.content;
            return {
                ...state,
                isLoggedIn: true,
                accessToken: accessToken,
                refreshToken: refreshToken
            }
        case LOGOUT:
            return {
                ...state,
                isLoggedIn: false,
            }
        case LOAD_USER:

            // RECEIVES accessToken and refreshToken
            return {
                ...state,
                isLoggedIn: false,
            }
        case ADD_TODO: {
            const { id, content } = action.payload;
            return {
                ...state,
                allIds: [...state.allIds, id],
                byIds: {
                    ...state.byIds,
                    [id]: {
                        content,
                        completed: false
                    }
                }
            };
        }
        case TOGGLE_TODO: {
            const { id } = action.payload;
            return {
                ...state,
                byIds: {
                    ...state.byIds,
                    [id]: {
                        ...state.byIds[id],
                        completed: !state.byIds[id].completed
                    }
                }
            };
        }
        default:
            return state;
    }
}
