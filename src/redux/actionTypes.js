export const ADD_TODO = "ADD_TODO";
export const TOGGLE_TODO = "TOGGLE_TODO";
export const SET_FILTER = "SET_FILTER";


export const LOAD_USER = 'smart-agent/auth/LOAD';
export const LOGIN = 'smart-agent/auth/LOGIN';
export const LOGIN_ASYNC = 'smart-agent/auth/LOGINASYNC';
export const LOGOUT = 'smart-agent/auth/LOGOUT';