
const endpoint = process.env.REACT_APP_API ? process.env.REACT_APP_API : "http://localhost:8080/"
const clientAPI = {
    doLogin: async (username, password) => {

        const response = await fetch(endpoint + 'auth', {
            method: "POST",
            body: JSON.stringify({
                username,
                password
            }),
            mode: "cors",
            credentials: "same-origin"
        });
        return await response.json();
    },
    doPost: async (serviceUrl, body) => {
        const response = await fetch((endpoint + serviceUrl), {
            method: "POST",
            body: JSON.stringify(body),
            credentials: 'same-origin',
            mode: 'no-cors'
        });
        return response;
    },
    doGet: async (serviceUrl, body) => {
        const response = await fetch((endpoint + serviceUrl), {
            method: "GET",
            body: JSON.stringify(body),
            credentials: 'same-origin',
            mode: 'no-cors'
        });
        return response;
    }
}

export default clientAPI;